const px2rem = require('postcss-px2rem');
const postcss = px2rem({
  remUnit: 75
});

// cdn预加载使用
console.log('process.env.NODE_ENV is: ' + process.env.NODE_ENV)

module.exports = {
  //设置打包后为相对路径
  publicPath: './',
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          postcss
        ]
      }
    }
  },
  chainWebpack: config => {
    // 这里是对环境的配置，不同环境对应不同的BASE_API，以便axios的请求地址不同
    config.plugin('define').tap(args => {
      const argv = process.argv
      const mode = argv[argv.indexOf('--project-mode') + 1]
      args[0]['process.env'].MODE = `"${mode}"`
      return args
    })
  },
  // 修改webpack config, 使其不打包externals下的资源
  configureWebpack: config => {
    const myConfig = {}
    // if (process.env.NODE_ENV === 'production') {
    //     // 1. 生产环境npm包转CDN
    //     myConfig.externals = externals
    // }
    if (process.env.NODE_ENV === 'development') {
      /**
       * 关闭host check，方便使用ngrok之类的内网转发工具
       */
      myConfig.devServer = {
        disableHostCheck: true
      }
    }
    return myConfig
  },
}

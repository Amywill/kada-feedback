import Vue from 'vue'
import App from './App.vue'
import TextareaAutosize from './plugin/vue-textarea-autosize/dist/vue-textarea-autosize.esm'

import './assets/css/reset.scss'

// Vue.config.productionTip = false

Vue.use(TextareaAutosize)

new Vue({
  render: h => h(App),
}).$mount('#app')

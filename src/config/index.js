import devConfig from './dev'
import betaConfig from './beta'
import preConfig from './pre'
import prodConfig from './prod'
let config={};
// console.log('===',process.env)
if(process.env.MODE === 'dev'){
  config = devConfig
}else if(process.env.MODE === 'beta'){
  config = betaConfig
}else if(process.env.MODE === 'pre'){
  config = preConfig
}else {
  config = prodConfig
}
// config.LOW_LEVEL = process.env.LOW_LEVEL
// console.log('===',config)

export default config

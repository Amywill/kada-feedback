import request from '@/utils/request'

let obj = {
  // 发送反馈的问题
  problemSend (formData, params) {
    return request({
      method: 'POST',
      data: formData,
      url: `/sys/feedback`,
      headers: {
        // 'Content-Type': 'application/x-www-form-urlencoded',
        'os': params.os,
        'version': params.version,
      }
    })
  }
}
export default obj

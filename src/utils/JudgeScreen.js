export default class JudgeScreen {
  static bigScreen() {
    // window.screen.height 为屏幕高度
    // window.screen.availHeight 为浏览器 可用高度
    let result = false;
    const rate = window.screen.height / window.screen.width;    
    let limit =  window.screen.height == window.screen.availHeight ? 1.8 : 1.65; // 临界判断值  
    if (rate > limit) {
      result = true;
    }
    // console.log('是全面屏js：'+result)
    return result;
  }
}
